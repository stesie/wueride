<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $email = null;

    #[ORM\Column(length: 30, unique: true)]
    private ?string $username = null;

    #[ORM\Column(length: 255)]
    private ?string $passwordHash = null;

    #[ORM\OneToMany(mappedBy: "owner", targetEntity: Token::class, cascade: ["persist", "remove"], orphanRemoval: true)]
    private Collection $tokens;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $registrationComplete;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $publicKey = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $privateKey = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $summary = null;

    #[ORM\OneToMany(mappedBy: 'follower', targetEntity: LocalFollow::class)]
    private Collection $localFollowees;

    #[ORM\OneToMany(mappedBy: 'followee', targetEntity: LocalFollow::class)]
    private Collection $localFollowers;

    #[ORM\OneToMany(mappedBy: 'localFollowee', targetEntity: ExternalFollower::class)]
    private Collection $externalFollowers;

    #[ORM\OneToMany(mappedBy: 'localFollower', targetEntity: ExternalFollowing::class)]
    private Collection $externalFollowees;

    public function __construct()
    {
        $this->tokens = new ArrayCollection();
        $this->localFollowees = new ArrayCollection();
        $this->localFollowers = new ArrayCollection();
        $this->externalFollowers = new ArrayCollection();
        $this->externalFollowees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    public function setPasswordHash(string $passwordHash): self
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->passwordHash;
    }

    public function getRoles(): array
    {
        return [];
    }

    public function eraseCredentials()
    {
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }


    /**
     * @return Collection|Token[]
     */
    public function getTokens(): Collection
    {
        return $this->tokens;
    }

    public function addToken(Token $token): self
    {
        if (!$this->tokens->contains($token)) {
            $this->tokens[] = $token;
            $token->setOwner($this);
        }

        return $this;
    }

    public function createToken(): Token
    {
        $token = Token::generate();
        $this->addToken($token);

        return $token;
    }

    public function removeToken(Token $token): self
    {
        if ($this->tokens->contains($token)) {
            $this->tokens->removeElement($token);
            // set the owning side to null (unless already changed)
            if ($token->getOwner() === $this) {
                $token->setOwner(null);
            }
        }

        return $this;
    }

    public function isRegistrationComplete(): bool
    {
        return $this->registrationComplete;
    }

    public function setRegistrationComplete(bool $registrationComplete): self
    {
        $this->registrationComplete = $registrationComplete;

        return $this;
    }

    public function getPublicKey(): ?string
    {
        return $this->publicKey;
    }

    public function setPublicKey(string $publicKey): self
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    public function getPrivateKey(): ?string
    {
        return $this->privateKey;
    }

    public function setPrivateKey(string $privateKey): self
    {
        $this->privateKey = $privateKey;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return Collection<int, LocalFollow>
     */
    public function getLocalFollowees(): Collection
    {
        return $this->localFollowees;
    }

    public function addLocalFollowee(LocalFollow $localFollowee): self
    {
        if (!$this->localFollowees->contains($localFollowee)) {
            $this->localFollowees->add($localFollowee);
            $localFollowee->setFollower($this);
        }

        return $this;
    }

    public function removeLocalFollowee(LocalFollow $localFollowee): self
    {
        if ($this->localFollowees->removeElement($localFollowee)) {
            // set the owning side to null (unless already changed)
            if ($localFollowee->getFollower() === $this) {
                $localFollowee->setFollower(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, LocalFollow>
     */
    public function getLocalFollowers(): Collection
    {
        return $this->localFollowers;
    }

    public function addLocalFollower(LocalFollow $localFollower): self
    {
        if (!$this->localFollowers->contains($localFollower)) {
            $this->localFollowers->add($localFollower);
            $localFollower->setFollowee($this);
        }

        return $this;
    }

    public function removeLocalFollower(LocalFollow $localFollower): self
    {
        if ($this->localFollowers->removeElement($localFollower)) {
            // set the owning side to null (unless already changed)
            if ($localFollower->getFollowee() === $this) {
                $localFollower->setFollowee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ExternalFollower>
     */
    public function getExternalFollowers(): Collection
    {
        return $this->externalFollowers;
    }

    public function addExternalFollower(ExternalFollower $externalFollower): self
    {
        if (!$this->externalFollowers->contains($externalFollower)) {
            $this->externalFollowers->add($externalFollower);
            $externalFollower->setLocalFollowee($this);
        }

        return $this;
    }

    public function removeExternalFollower(ExternalFollower $externalFollower): self
    {
        if ($this->externalFollowers->removeElement($externalFollower)) {
            // set the owning side to null (unless already changed)
            if ($externalFollower->getLocalFollowee() === $this) {
                $externalFollower->setLocalFollowee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ExternalFollowing>
     */
    public function getExternalFollowees(): Collection
    {
        return $this->externalFollowees;
    }

    public function addExternalFollowee(ExternalFollowing $externalFollowee): self
    {
        if (!$this->externalFollowees->contains($externalFollowee)) {
            $this->externalFollowees->add($externalFollowee);
            $externalFollowee->setLocalFollower($this);
        }

        return $this;
    }

    public function removeExternalFollowee(ExternalFollowing $externalFollowee): self
    {
        if ($this->externalFollowees->removeElement($externalFollowee)) {
            // set the owning side to null (unless already changed)
            if ($externalFollowee->getLocalFollower() === $this) {
                $externalFollowee->setLocalFollower(null);
            }
        }

        return $this;
    }
}

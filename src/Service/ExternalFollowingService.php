<?php

namespace App\Service;

use ActivityPhp\Type;
use App\Entity\ExternalFollowing;
use App\Repository\ExternalFollowingRepository;

class ExternalFollowingService
{


    private RequestService $requestService;
    private ExternalFollowingRepository $externalFollowingRepository;
    private OutboxService $outboxService;
    private LocalActorService $localActorService;

    public function __construct(RequestService $requestService, ExternalFollowingRepository $externalFollowingRepository, OutboxService $outboxService, LocalActorService $localActorService)
    {
        $this->requestService = $requestService;
        $this->externalFollowingRepository = $externalFollowingRepository;
        $this->outboxService = $outboxService;
        $this->localActorService = $localActorService;
    }

    public function follow(\App\Entity\ExternalActor $followee)
    {
        $follower = $this->requestService->getLoggedInUser();

        if ($this->externalFollowingRepository->isFollowing($follower, $followee)) {
            return;
        }

        $externalFollowing = (new ExternalFollowing())
            ->setLocalFollower($follower)
            ->setExternalActor($followee);
        $this->externalFollowingRepository->save($externalFollowing, true);

        $follow = Type::create('Follow', [
            "@context" => "https://www.w3.org/ns/activitystreams",
            'id' => \sprintf('https://%s/external-following/%d', $this->requestService->getDomain(), $externalFollowing->getId()),
            'actor' => $this->localActorService->getActorUri($follower),
            'object' => $followee->getExternalId(),
        ]);

        $this->outboxService->publish($follower, $followee, $follow);
        /*             /* {"@context":"https://www.w3.org/ns/activitystreams","id":"https://wue.social/777d2ef9-87ef-485e-8479-af61266b99c2",
               "type":"Follow","actor":"https://wue.social/users/rolf","object":"https://white-robin-23.telebit.io/@dev"} */

    }
}
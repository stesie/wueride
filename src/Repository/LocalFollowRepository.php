<?php

namespace App\Repository;

use App\Entity\LocalFollow;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LocalFollow>
 *
 * @method LocalFollow|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocalFollow|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocalFollow[]    findAll()
 * @method LocalFollow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalFollowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocalFollow::class);
    }

    public function add(User $follower, User $followee, bool $flush = false): void
    {
        $entity = (new LocalFollow())->setFollower($follower)->setFollowee($followee);
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $follower, User $followee): void
    {
        $this->_em->createQueryBuilder()
            ->delete('App:LocalFollow', 'lf')
            ->andWhere('lf.follower = :follower')
            ->setParameter('follower', $follower)
            ->andWhere('lf.followee = :followee')
            ->setParameter('followee', $followee)
            ->getQuery()
            ->execute();
    }

//    /**
//     * @return LocalFollow[] Returns an array of LocalFollow objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?LocalFollow
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
    public function isFollowing(User $follower, User $followee)
    {
        $localFollow = $this->createQueryBuilder('lf')
            ->andWhere('lf.follower = :follower')
            ->setParameter('follower', $follower)
            ->andWhere('lf.followee = :followee')
            ->setParameter('followee', $followee)
            ->getQuery()
            ->getOneOrNullResult();
        return $localFollow !== null;
    }
}

<?php

namespace App\Entity;

use App\Repository\ExternalPostRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExternalPostRepository::class)]
class ExternalPost extends Post
{

    #[ORM\Column(length: 255)]
    private ?string $url = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?ExternalActor $attributedTo = null;



    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAttributedTo(): ?ExternalActor
    {
        return $this->attributedTo;
    }

    public function setAttributedTo(?ExternalActor $attributedTo): self
    {
        $this->attributedTo = $attributedTo;

        return $this;
    }

}

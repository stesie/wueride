<?php

namespace App\Controller;

use ActivityPhp\Server;
use ActivityPhp\Type;
use App\DTO\Actor;
use App\Entity\LocalFollow;
use App\Entity\User;
use App\Repository\LocalFollowRepository;
use App\Service\ContentNegotiator;
use App\Service\InboxHandlerService;
use App\Service\LocalFollowService;
use App\Service\RequestService;
use Negotiation\Exception\InvalidMediaType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends AbstractController
{
    private LoggerInterface $logger;
    private ContentNegotiator $contentNegotiator;
    private LocalFollowRepository $localFollowRepository;
    private RequestService $requestService;

    public function __construct(ContentNegotiator $contentNegotiator, LoggerInterface $logger,
                                LocalFollowRepository $localFollowRepository, RequestService $requestService)
    {
        $this->logger = $logger;
        $this->contentNegotiator = $contentNegotiator;
        $this->localFollowRepository = $localFollowRepository;
        $this->requestService = $requestService;
    }


    private function collectProfileBoxInfo(User $user)
    {
        $actor = (new Actor())
            ->setDisplayName($user->getName())
            ->setUsername($user->getUsername())
            ->setShortHandle($user->getUsername())
            ->setHandle(\implode('@', [$user->getUsername(), $this->requestService->getDomain()]))
            ->setSummary($user->getSummary())
            ->setFollowerCount($user->getLocalFollowers()->count() + $user->getExternalFollowers()->count())
            ->setFollowingCount($user->getLocalFollowees()->count());

        if (($loggedInUser = $this->requestService->getLoggedInUser())) {
            $actor
                ->setUserIsFollowing($this->localFollowRepository->isFollowing($loggedInUser, $user))
                ->setIsFollowingUser($this->localFollowRepository->isFollowing($user, $loggedInUser));
        }

        return [
            'actor' => $actor
        ];
    }

    #[Route("/@{username}", name: 'user_profile', condition: "not (params['username'] contains '@')")]
    public function userProfile(Request $request, User $user): Response
    {

        if (!$this->contentNegotiator->isActivityPubRequest()) {
            return $this->render('user/profile.html.twig', $this->collectProfileBoxInfo($user));
        }

        $profile = Type::create('Person', [
            '@context' => [
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1"
            ],

            "id" => $request->getUri(),
            "following" => \sprintf('%s/following', $request->getUri()),
            "followers" => \sprintf('%s/followers', $request->getUri()),
            "inbox" => \sprintf('%s/inbox', $request->getUri()),
            "outbox" => \sprintf('%s/outbox', $request->getUri()),
            "preferredUsername" => $user->getUsername(),
            "name" => $user->getName(),
            "summary" => $user->getSummary(),
            "url" => $request->getUri(),
            // "manuallyApprovesFollowers" => false,
            "publicKey" => [
                "id" => \sprintf('%s#main-key', $request->getUri()),
                "owner" => $request->getUri(),
                "publicKeyPem" => $user->getPublicKey()
            ],
        ]);

        return new JsonResponse($profile->toArray());
        /* {
  "@context": [
    ,
    {
      "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
      "toot": "http://joinmastodon.org/ns#",
      "featured": {
        "@id": "toot:featured",
        "@type": "@id"
      },
      "featuredTags": {
        "@id": "toot:featuredTags",
        "@type": "@id"
      },
      "alsoKnownAs": {
        "@id": "as:alsoKnownAs",
        "@type": "@id"
      },
      "movedTo": {
        "@id": "as:movedTo",
        "@type": "@id"
      },
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "discoverable": "toot:discoverable",
      "Device": "toot:Device",
      "Ed25519Signature": "toot:Ed25519Signature",
      "Ed25519Key": "toot:Ed25519Key",
      "Curve25519Key": "toot:Curve25519Key",
      "EncryptedMessage": "toot:EncryptedMessage",
      "publicKeyBase64": "toot:publicKeyBase64",
      "deviceId": "toot:deviceId",
      "claim": {
        "@type": "@id",
        "@id": "toot:claim"
      },
      "fingerprintKey": {
        "@type": "@id",
        "@id": "toot:fingerprintKey"
      },
      "identityKey": {
        "@type": "@id",
        "@id": "toot:identityKey"
      },
      "devices": {
        "@type": "@id",
        "@id": "toot:devices"
      },
      "messageFranking": "toot:messageFranking",
      "messageType": "toot:messageType",
      "cipherText": "toot:cipherText",
      "suspended": "toot:suspended",
      "Hashtag": "as:Hashtag",
      "focalPoint": {
        "@container": "@list",
        "@id": "toot:focalPoint"
      }
    }
  ],

  "featured": "https://wue.social/users/rolf/collections/featured",
  "featuredTags": "https://wue.social/users/rolf/collections/tags",

  "discoverable": true,
  "published": "2020-05-11T00:00:00Z",
  "devices": "https://wue.social/users/rolf/collections/devices",
  "alsoKnownAs": [
    "https://chaos.social/users/stesie"
  ],

  "tag": [
    {
      "type": "Hashtag",
      "href": "https://wue.social/tags/einautoweniger",
      "name": "#einautoweniger"
    }
  ],
  "attachment": [],
  "endpoints": {
    "sharedInbox": "https://wue.social/inbox"
  },
  "icon": {
    "type": "Image",
    "mediaType": "image/jpeg",
    "url": "https://wue.social/system/accounts/avatars/000/002/048/original/b39938a92067dee2.jpg"
  }
} */
    }

    #[Route("/@{username}/follow", name: 'user_follow', methods: 'POST', condition: "not (params['username'] contains '@')")]
    public function follow(Request $request, User $user, LocalFollowService $localFollowService)
    {
        if ($this->isCsrfTokenValid('follow' . $user->getUsername(), $request->request->get('_token'))) {
            throw new AccessDeniedException('CSRF token invalid');
        }

        $localFollowService->follow($user);
        return $this->redirectToRoute('user_profile', ['username' => $user->getUsername()]);
    }


    #[Route("/@{username}/undo-follow", name: 'user_undo_follow', methods: 'POST', condition: "not (params['username'] contains '@')")]
    public function undoFollow(Request $request, User $user, LocalFollowService $localFollowService)
    {
        if ($this->isCsrfTokenValid('undo-follow' . $user->getUsername(), $request->request->get('_token'))) {
            throw new AccessDeniedException('CSRF token invalid');
        }

        $localFollowService->unfollow($user);
        return $this->redirectToRoute('user_profile', ['username' => $user->getUsername()]);
    }

    #[Route("/@{username}/followers", name: 'user_followers', condition: "not (params['username'] contains '@')")]
    public function userFollowers(User $user): Response
    {
        // TODO this should also list external followers

        return $this->render('user/relations.html.twig', array_merge(
            $this->collectProfileBoxInfo($user), [
            'relations' => $user->getLocalFollowers()->map(function (LocalFollow $lf) {
                return $lf->getFollower();
            }),
        ]));
    }


    #[Route("/@{username}/following", name: 'user_following', condition: "not (params['username'] contains '@')")]
    public function userFollowing(User $user): Response
    {
        return $this->render('user/relations.html.twig', array_merge(
            $this->collectProfileBoxInfo($user), [
            'relations' => $user->getLocalFollowees()->map(function (LocalFollow $lf) {
                return $lf->getFollowee();
            }),
        ]));
    }

    #[Route("/@{username}/inbox", methods: ['POST'], condition: "not (params['username'] contains '@')")]
    public function inbox(Request $request, User $user, InboxHandlerService $inboxHandlerService): Response
    {

        $server = new Server([]);
        Type\TypeConfiguration::set('undefined_properties', 'ignore');
        $httpSignature = new Server\Http\HttpSignature($server);
        $signatureVerifyResult = $httpSignature->verify($request);
        $this->logger->info("Signature check status", ['valid' => $signatureVerifyResult]);

        if ($signatureVerifyResult !== true) {
            throw new BadRequestHttpException('Invalid HTTP signature');
        }

        $this->logger->info('received inbox body: ' . $request->getContent(), ['headers' => $request->headers]);

        if ($request->headers->get('CONTENT_TYPE') !== 'application/activity+json') {
            throw new InvalidMediaType('Unsupported content type');
        }

        $message = Type::fromJson($request->getContent());
        $inboxHandlerService->handleMessage($user, $message);

        return new Response();
    }
}
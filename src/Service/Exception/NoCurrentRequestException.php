<?php

namespace App\Service\Exception;

class NoCurrentRequestException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Cannot negotiate content without active request');
    }

}
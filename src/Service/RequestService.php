<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RequestService
{
    private TokenStorageInterface $tokenStorage;
    private RequestStack $requestStack;

    public function __construct(TokenStorageInterface $tokenStorage, RequestStack $requestStack)
    {
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    public function getLoggedInUser(): ?User
    {
        if ($this->tokenStorage->getToken() === null || $this->tokenStorage->getToken()->getUser() === null) {
            return null;
        }

        $loggedInUser = $this->tokenStorage->getToken()->getUser();

        if (!$loggedInUser instanceof User) {
            throw new \LogicException('$loggedInUser must be of User type');
        }

        return $loggedInUser;
    }

    public function getDomain(): string
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request === null) {
            throw new \LogicException('currentRequest cannot be null here.');
        }

        return $request->getHttpHost();
    }
}
<?php

namespace App\Service;

use App\Entity\User;

class LocalActorService
{

    private RequestService $requestService;

    public function __construct(RequestService $requestService)
    {
        $this->requestService = $requestService;
    }

    public function getActorUri(User $user): string
    {
        return \sprintf('https://%s/@%s', $this->requestService->getDomain(), $user->getUsername());
    }
}
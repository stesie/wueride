<?php

if ('white-robin-23.telebit.io' === $_SERVER['HTTP_HOST']) {
    $_SERVER['HTTPS'] = 'on';
}

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};

<?php

namespace App\Entity;

use App\Repository\ExternalFollowerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExternalFollowerRepository::class)]
class ExternalFollower
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?ExternalActor $externalActor = null;

    #[ORM\ManyToOne(inversedBy: 'externalFollowers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $localFollowee = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalActor(): ?ExternalActor
    {
        return $this->externalActor;
    }

    public function setExternalActor(?ExternalActor $externalActor): self
    {
        $this->externalActor = $externalActor;

        return $this;
    }

    public function getLocalFollowee(): ?User
    {
        return $this->localFollowee;
    }

    public function setLocalFollowee(?User $localFollowee): self
    {
        $this->localFollowee = $localFollowee;

        return $this;
    }
}

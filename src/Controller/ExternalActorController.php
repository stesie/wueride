<?php

namespace App\Controller;

use ActivityPhp\Server\Http\Request;
use ActivityPhp\Type;
use App\DTO\Actor;
use App\Service\ExternalActorService;
use App\Service\ExternalFollowingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ExternalActorController extends AbstractController
{
    private ExternalActorService $externalActorService;
    private ExternalFollowingService $remoteFollowService;

    public function __construct(ExternalActorService $externalActorService, ExternalFollowingService $remoteFollowService)
    {
        $this->externalActorService = $externalActorService;
        $this->remoteFollowService = $remoteFollowService;
    }

    #[Route("/@{handle}", name: 'external_actor_profile', condition: "params['handle'] contains '@'")]
    public function profile(string $handle): Response
    {
        $externalActor = $this->externalActorService->fetchActor($handle);

        $actor = (new Actor())
            ->setDisplayName($externalActor->get('name'))
            ->setUsername($externalActor->get('preferredUsername'))
            ->setShortHandle($handle)
            ->setHandle($handle)
            ->setSummary(strip_tags($externalActor->get('summary')))
            ->setFollowerCount($this->fetchNumTotalItems($externalActor->get('followers')))
            ->setFollowingCount($this->fetchNumTotalItems($externalActor->get('following')));;

        return $this->render('user/profile.html.twig', ['actor' => $actor]);
    }

    private function fetchNumTotalItems(string $url): int
    {
        $payload = Type::fromJson((new Request())->get($url));

        if (!$payload instanceof Type\Core\OrderedCollection) {
            throw new \InvalidArgumentException('$url did not point to OrderedCollection');
        }

        return $payload->get('totalItems');
    }

    #[Route("/@{handle}/follow", name: 'external_actor_follow', methods: 'POST', condition: "params['handle'] contains '@'")]
    public function follow(\Symfony\Component\HttpFoundation\Request $request, string $handle)
    {
        if ($this->isCsrfTokenValid('follow' . $handle, $request->request->get('_token'))) {
            throw new AccessDeniedException('CSRF token invalid');
        }

        $externalActor = $this->externalActorService->get($handle);
        $this->remoteFollowService->follow($externalActor);
        return $this->redirectToRoute('external_actor_profile', ['handle' => $externalActor->getHandle()]);
    }
}
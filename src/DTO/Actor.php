<?php

namespace App\DTO;

class Actor
{
    private string $displayName;

    private string $username;

    private string $shortHandle;

    private string $handle;

    private ?string $summary;

    private int $followerCount;

    private int $followingCount;

    private ?bool $userIsFollowing = null;

    private ?bool $isFollowingUser = null;

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return Actor
     */
    public function setDisplayName(string $displayName): Actor
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Actor
     */
    public function setUsername(string $username): Actor
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortHandle(): string
    {
        return $this->shortHandle;
    }

    /**
     * @param string $shortHandle
     * @return Actor
     */
    public function setShortHandle(string $shortHandle): Actor
    {
        $this->shortHandle = $shortHandle;
        return $this;
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @param string $handle
     * @return Actor
     */
    public function setHandle(string $handle): Actor
    {
        $this->handle = $handle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return Actor
     */
    public function setSummary(?string $summary): Actor
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowerCount(): int
    {
        return $this->followerCount;
    }

    /**
     * @param int $followerCount
     * @return Actor
     */
    public function setFollowerCount(int $followerCount): Actor
    {
        $this->followerCount = $followerCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowingCount(): int
    {
        return $this->followingCount;
    }

    /**
     * @param int $followingCount
     * @return Actor
     */
    public function setFollowingCount(int $followingCount): Actor
    {
        $this->followingCount = $followingCount;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getUserIsFollowing(): ?bool
    {
        return $this->userIsFollowing;
    }

    /**
     * @param bool|null $userIsFollowing
     * @return Actor
     */
    public function setUserIsFollowing(?bool $userIsFollowing): Actor
    {
        $this->userIsFollowing = $userIsFollowing;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsFollowingUser(): ?bool
    {
        return $this->isFollowingUser;
    }

    /**
     * @param bool|null $isFollowingUser
     * @return Actor
     */
    public function setIsFollowingUser(?bool $isFollowingUser): Actor
    {
        $this->isFollowingUser = $isFollowingUser;
        return $this;
    }



}
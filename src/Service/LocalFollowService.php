<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\LocalFollowRepository;

class LocalFollowService
{
    private RequestService $requestService;
    private LocalFollowRepository $localFollowRepository;

    public function __construct(RequestService $requestService, LocalFollowRepository $localFollowRepository)
    {
        $this->requestService = $requestService;
        $this->localFollowRepository = $localFollowRepository;
    }

    public function follow(User $followee)
    {
        $this->localFollowRepository->add($this->requestService->getLoggedInUser(), $followee, true);
    }

    public function unfollow(User $followee)
    {
        $this->localFollowRepository->remove($this->requestService->getLoggedInUser(), $followee);
    }

}
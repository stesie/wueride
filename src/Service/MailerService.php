<?php

namespace App\Service;

use App\Entity\Token;
use App\Entity\User;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class MailerService
{
    const FROM_ADDRESS = 'noreply@wueride.bike';

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(Environment $twig, MailerInterface $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    public function sendUserRegistrationMail(User $user, Token $token): void
    {
        $message = (new Email())
            ->from(self::FROM_ADDRESS)
            ->to($user->getEmail())
            ->subject('Deine Registrierung bei wueride.bike')
            ->text(
                $this->twig->render('emails/user_registration.txt.twig', [
                    'user' => $user,
                    'token' => $token->getToken(),
                ])
            );

        $this->mailer->send($message);
    }

    public function sendPasswordResetMail(?User $user, Token $token): void
    {
        $message = (new Email())
            ->from(self::FROM_ADDRESS)
            ->to($user->getEmail())
            ->subject('wueride.bike Passwort zurücksetzen')
            ->text(
                $this->twig->render('emails/password_reset.txt.twig', [
                    'user' => $user,
                    'token' => $token->getToken(),
                ])
            );

        $this->mailer->send($message);
    }

}

<?php

namespace App\Service;

use ActivityPhp\Type;
use ActivityPhp\Type\AbstractObject;
use ActivityPhp\Type\Extended\Activity\Create;
use ActivityPhp\Type\Extended\Activity\Follow;
use ActivityPhp\Type\Extended\Activity\Undo;
use ActivityPhp\Type\Extended\Object\Note;
use App\Entity\ExternalActor;
use App\Entity\ExternalPost;
use App\Entity\User;
use App\Repository\ExternalFollowerRepository;
use App\Repository\ExternalPostRepository;
use Psr\Log\LoggerInterface;

class InboxHandlerService
{
    private LoggerInterface $logger;
    private ExternalActorService $externalActorService;
    private ExternalFollowerRepository $externalFollowerRepository;
    private LocalActorService $localActorService;
    private OutboxService $outboxService;
    private ExternalPostRepository $externalPostRepository;

    public function __construct(LoggerInterface            $logger,
                                ExternalActorService       $externalActorService,
                                ExternalFollowerRepository $externalFollowerRepository,
                                LocalActorService          $localActorService,
                                OutboxService              $outboxService,
                                ExternalPostRepository     $externalPostRepository)
    {
        $this->logger = $logger;
        $this->externalActorService = $externalActorService;
        $this->externalFollowerRepository = $externalFollowerRepository;
        $this->localActorService = $localActorService;
        $this->outboxService = $outboxService;
        $this->externalPostRepository = $externalPostRepository;
    }

    public function handleMessage(User $user, AbstractObject $message)
    {
        if ($message instanceof Follow) {
            /* {"@context":"https://www.w3.org/ns/activitystreams","id":"https://wue.social/777d2ef9-87ef-485e-8479-af61266b99c2",
               "type":"Follow","actor":"https://wue.social/users/rolf","object":"https://white-robin-23.telebit.io/@dev"} */
            $this->handleFollow($user, $message);
            return;
        }

        if ($message instanceof Undo) {
            $object = $message->get('object');
            $this->logger->debug('received undo', ['object' => $object]);

            if ($object instanceof Follow) {
                /* {"@context":"https://www.w3.org/ns/activitystreams","id":"https://wue.social/users/rolf#follows/2108/undo",
                "type":"Undo","actor":"https://wue.social/users/rolf","object":{"id":"https://wue.social/777d2ef9-87ef-485e-8479-af61266b99c2",
                "type":"Follow","actor":"https://wue.social/users/rolf","object":"https://white-robin-23.telebit.io/@dev"}} */
                $this->handleUndoFollow($user, $this->externalActorService->get($message->get('actor')));
            }
        }

        if ($message instanceof Create) {
            $object = $message->get('object');

            if ($object instanceof Note) {
                $this->handleCreateNote($user, $this->externalActorService->get($message->get('actor')), $object);
            }
        }
    }

    private function handleFollow(User $user, Follow $message)
    {
        $externalActor = $this->externalActorService->get($message->get('actor'));
        $externalFollowing = $this->externalFollowerRepository->add($externalActor, $user, true);

        $actorUri = $this->localActorService->getActorUri($user);
        $accept = Type::create('Accept', [
            "@context" => "https://www.w3.org/ns/activitystreams",
            "id" => \sprintf("%s#accepts/externalFollower/%d", $actorUri, $externalFollowing->getId()),
            "actor" => $actorUri,
            "object" => $message,
        ]);
        $this->outboxService->publish($user, $externalActor, $accept);
    }

    private function handleUndoFollow(User $user, ExternalActor $externalActor)
    {
        $this->externalFollowerRepository->remove($externalActor, $user);
    }

    private function handleCreateNote(User $receiver, ExternalActor $author, Note $post)
    {
        // TODO check if note already exists

        $post = (new ExternalPost())
            ->setUri($post->get('id'))
            ->setPublished(new \DateTimeImmutable($post->get('published')))
            ->setContent($post->get('content'))
            ->setUrl($post->get('url'))
            ->setAttributedTo($author);
        $this->externalPostRepository->save($post, true);
    }
}
<?php

namespace App\Service;

use App\Entity\ExternalActor;
use App\Entity\User;
use GuzzleHttp\Psr7\Request;
use phpseclib3\Crypt\RSA;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;

class OutboxService
{
    private LoggerInterface $logger;
    private ClientInterface $client;
    private ExternalActorService $externalActorService;
    private RequestService $requestService;

    public function __construct(LoggerInterface $logger, ClientInterface $client, ExternalActorService $externalActorService, RequestService $requestService)
    {
        $this->logger = $logger;
        $this->client = $client;
        $this->externalActorService = $externalActorService;
        $this->requestService = $requestService;
    }

    public function publish(User $signer, ExternalActor $receiver, \ActivityPhp\Type\AbstractObject $message)
    {
        $this->logger->debug('about to publish message', ['receiver' => $receiver->getExternalId(), 'message' => $message->toArray()]);

        $actor = $this->externalActorService->fetchActor($receiver->getExternalId());
        $inboxUri = $actor->get('inbox');
        $this->logger->debug('using inbox', ['uri' => $inboxUri]);

        $body = $message->toJson();
        $request = new Request('POST', $inboxUri, [
            'Content-Type' => 'application/activity+json',
            'Date' => \gmdate("D, d M Y H:i:s T"),
            'Digest' => \sprintf('sha-256=%s', \base64_encode(\hash('sha256', $body, true)))
        ], $body);

        $signedRequest = $this->signRequest($signer, $request);
        $response = $this->client->sendRequest($signedRequest);

        $this->logger->debug('server response', ['statusCode' => $response->getStatusCode(), 'body' => $response->getBody()->getContents()]);
    }

    private function signRequest(User $signer, Request $request): Request
    {
        [$keys, $plainText] = $this->getPlainTextToSign($request);

        /** @var RSA\PrivateKey $rsa */
        $rsa = RSA::load($signer->getPrivateKey())->withHash('sha256')->withPadding(RSA::SIGNATURE_PKCS1);
        $signature = $rsa->sign($plainText);

        $keyId = \sprintf('https://%s/@%s#main-key', $this->requestService->getDomain(), $signer->getUsername());
        $signatureHeader = \sprintf('keyId="%s",algorithm="rsa-sha256",headers="%s",signature="%s"', $keyId, \implode(' ', $keys), \base64_encode($signature));

        $signedRequest = $request->withHeader('signature', $signatureHeader);

        if (!$signedRequest instanceof  Request) {
            throw new \LogicException('withHeader should not change class');
        }

        return $signedRequest;
    }

    private function getPlainTextToSign(Request $request)
    {
        $keys = ['(request-target)'];
        $strings = [];
        $strings[] = \sprintf(
            '(request-target): %s %s%s',
            strtolower($request->getMethod()),
            $request->getUri()->getPath(),
            $request->getUri()->getQuery()
                ? '?' . $request->getUri()->getQuery() : ''
        );

        foreach ($request->getHeaders() as $key => $value) {
            $keys[] = \strtolower($key);
            $strings[] = \sprintf('%s: %s', \strtolower($key), \implode(', ', $value));
        }

        return [$keys, \implode("\n", $strings)];
    }
}
<?php

namespace App\Entity;

use App\Repository\ExternalFollowingRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExternalFollowingRepository::class)]
class ExternalFollowing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'externalFollowees')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $localFollower = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?ExternalActor $externalActor = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocalFollower(): ?User
    {
        return $this->localFollower;
    }

    public function setLocalFollower(?User $localFollower): self
    {
        $this->localFollower = $localFollower;

        return $this;
    }

    public function getExternalActor(): ?ExternalActor
    {
        return $this->externalActor;
    }

    public function setExternalActor(?ExternalActor $externalActor): self
    {
        $this->externalActor = $externalActor;

        return $this;
    }
}

<?php

namespace App\Service;

use App\Service\Exception\NoCurrentRequestException;
use Negotiation\AcceptHeader;
use Negotiation\Negotiator;
use Symfony\Component\HttpFoundation\RequestStack;

class ContentNegotiator
{
    const CONTENT_TYPE_ACTIVITY_PUB = 'application/activity+json';
    const CONTENT_TYPE_HTML = 'text/html; charset=UTF-8';

    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    private function negotiateContent(array $priorities): ?AcceptHeader
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request === null) {
            throw new NoCurrentRequestException();
        }

        $acceptHeader = $request->headers->get('accept');

        if ($acceptHeader === null) {
            return null;
        }

        $negotiator = new Negotiator();
        return $negotiator->getBest($acceptHeader, $priorities);
    }

    public function isActivityPubRequest(): bool
    {
        $match = $this->negotiateContent([self::CONTENT_TYPE_HTML, self::CONTENT_TYPE_ACTIVITY_PUB]);

        return $match !== null && $match->getValue() === self::CONTENT_TYPE_ACTIVITY_PUB;
    }
}
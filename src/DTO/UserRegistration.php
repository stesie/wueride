<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class UserRegistration
{
    private string $username;

    private string $name;

    private string $email;

    #[Assert\Length(min: 8, minMessage: "Das Passwort muss mindestens acht Zeichen lang sein.")]
    private string $password;


    public function getUsername(): ?string
    {
        return $this->username;
    }


    public function setUsername(string $username): UserRegistration
    {
        $this->username = $username;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): UserRegistration
    {
        $this->name = $name;
        return $this;
    }



    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): UserRegistration
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): UserRegistration
    {
        $this->password = $password;
        return $this;
    }
}

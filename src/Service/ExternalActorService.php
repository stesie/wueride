<?php

namespace App\Service;

use ActivityPhp\Server;
use ActivityPhp\Type;
use ActivityPhp\Type\Extended\AbstractActor;
use App\Entity\ExternalActor;
use App\Repository\ExternalActorRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExternalActorService
{
    private ExternalActorRepository $externalActorRepository;

    public function __construct(ExternalActorRepository $externalActorRepository)
    {
        $this->externalActorRepository = $externalActorRepository;
    }

    public function get(string $handle): ExternalActor
    {
        if (self::isHandle($handle)) {
            $externalActor = $this->externalActorRepository->findOneBy(['handle' => $handle]);
        } elseif (Type\Util::validateUrl($handle)) {
            $externalActor = $this->externalActorRepository->findOneBy(['externalId' => $handle]);
        } else {
            throw new \RuntimeException('Bad external actor handle: ' . $handle);
        }

        if ($externalActor === null) {
            $remoteInfo = $this->fetchActor($handle);

            $externalActor = (new ExternalActor())
                ->setExternalId($remoteInfo->get('id'))
                ->setHandle($this->deriveHandle($remoteInfo));
            $this->externalActorRepository->add($externalActor, true);
        }

        return $externalActor;
    }

    public function fetchActor(string $handle): AbstractActor
    {
        $server = new Server([]);
        Type\TypeConfiguration::set('undefined_properties', 'ignore');

        try {
            return $server->actor($handle)->get();
        } catch (\Exception $ex) {
            if (str_contains($ex->getMessage(), '404 Not Found')) {
                throw new NotFoundHttpException('External Actor not found', $ex);
            }

            throw $ex;
        }
    }

    /**
     * Check that a string is a valid handle
     *
     * @param string $handle
     * @return bool
     */
    private static function isHandle(string $handle)
    {
        return (bool)preg_match(
            '/^@?(?P<user>[\w\.\-]+)@(?P<host>[\w\.\-]+)(?P<port>:[\d]+)?$/',
            $handle
        );
    }

    private function deriveHandle(AbstractActor $remoteInfo): string
    {
        $scheme = parse_url($remoteInfo->get('id'), PHP_URL_SCHEME);
        $host = parse_url($remoteInfo->get('id'), PHP_URL_HOST);
        $port = parse_url($remoteInfo->get('id'), PHP_URL_PORT);

        if ($port == 443 && $scheme == 'https') {
            $port = null;
        }

        if ($port == 80 && $scheme == 'http') {
            $port = null;
        }

        return \sprintf('%s@%s%s', $remoteInfo->get('preferredUsername'), $host, is_null($port) ? '' : ":{$port}");
    }
}
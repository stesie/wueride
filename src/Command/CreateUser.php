<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserService;
use phpseclib3\Crypt\RSA;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUser extends Command
{

    private UserService $userService;
    private UserRepository $userRepository;

    public function __construct(UserService $userService, UserRepository $userRepository)
    {
        parent::__construct();
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this->setName('app:create-user')
            ->setDescription('Create a user')
            ->addArgument('email', InputArgument::REQUIRED, 'email address of the new user')
            ->addArgument('username', InputArgument::REQUIRED, 'username of the new user')
            ->addArgument('password', InputArgument::REQUIRED, 'password of the new user')
            ->addArgument('name', InputArgument::REQUIRED, '(full/display) name of the new user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $key = RSA::createKey();
        $privateKeyString = \str_replace("\r", "", $key->toString('PKCS8'));
        $publicKeyString = \str_replace("\r", "", $key->getPublicKey()->toString('PKCS8'));

        $user = (new User())
            ->setEmail($input->getArgument('email'))
            ->setUsername($input->getArgument('username'))
            ->setName($input->getArgument('name'))
            ->setPublicKey($publicKeyString)
            ->setPrivateKey($privateKeyString);

        $this->userService->changePassword($user, $input->getArgument('password'));
        $this->userRepository->save($user, true);

        return 0;
    }


}
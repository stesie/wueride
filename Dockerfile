FROM php:8.1-cli AS builder

# install composer to /composer.phar
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y git unzip nodejs
RUN npm -g install yarn

# copy app sources
COPY / /app
WORKDIR /app

RUN php /composer.phar install

RUN yarn install
RUN yarn build

#------------------------------------------------------------------------------

FROM php:8.1-apache

ENV APACHE_DOCUMENT_ROOT /app/public

RUN docker-php-ext-install pdo pdo_mysql

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && \
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf && \
    sed -ri -e 's!Options Indexes FollowSymLinks!FallbackResource /index.php!' /etc/apache2/apache2.conf

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
    echo 'date.timezone = "Europe/Berlin"' > $PHP_INI_DIR/conf.d/timezone.ini && \
    echo 'variables_order = "GPCSE"' > $PHP_INI_DIR/conf.d/variables-order.ini && \
    echo 'upload_max_filesize = 8M' > $PHP_INI_DIR/conf.d/upload_max_filesize.ini && \
    echo 'post_max_size = 10M' > $PHP_INI_DIR/conf.d/post_max_size.ini && \
    echo memory_limit = 128M > $PHP_INI_DIR/conf.d/memory-limit.ini

COPY --from=builder /app /app
WORKDIR /app

ENV APP_ENV=prod
RUN bin/console cache:warmup -n && \
    chown -R www-data. var/

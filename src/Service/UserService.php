<?php

namespace App\Service;

use App\DTO\FinishPasswordReset;
use App\DTO\StartPasswordReset;
use App\DTO\UserRegistration;
use App\Entity\User;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use App\Service\Exception\PasswordIsPwnedException;
use App\Service\Exception\TokenNotFoundException;
use App\Service\Exception\UsernameNotUniqueException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use phpseclib3\Crypt\RSA;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{

    private UserRepository $userRepository;
    private MailerService $mailerService;
    private UserPasswordHasherInterface $passwordHasher;
    private PwnedService $pwnedService;
    private TokenRepository $tokenRepository;

    public function __construct(
        UserRepository              $userRepository,
        MailerService               $mailerService,
        UserPasswordHasherInterface $passwordHasher,
        PwnedService                $pwnedService,
        TokenRepository             $tokenRepository
    ) {
        $this->userRepository = $userRepository;
        $this->mailerService = $mailerService;
        $this->passwordHasher = $passwordHasher;
        $this->pwnedService = $pwnedService;
        $this->tokenRepository = $tokenRepository;
    }


    public function registerUser(UserRegistration $dto): void
    {
        $key = RSA::createKey();
        $privateKeyString = \str_replace("\r", "", $key->toString('PKCS8'));
        $publicKeyString = \str_replace("\r", "", $key->getPublicKey()->toString('PKCS8'));

        $user = new User();

        $user
            ->setUsername($dto->getUsername())
            ->setName($dto->getName())
            ->setEmail($dto->getEmail())
            ->setRegistrationComplete(false)
            ->setPrivateKey($privateKeyString)
            ->setPublicKey($publicKeyString);

        $this->changePassword($user, $dto->getPassword());

        $token = $user->createToken();

        try {
            $this->userRepository->save($user, true);
        } catch (UniqueConstraintViolationException $ex) {
            throw new UsernameNotUniqueException($ex);
        }

        $this->mailerService->sendUserRegistrationMail($user, $token);
    }

    public function finishRegistration(string $token): void
    {
        $tokenEntity = $this->tokenRepository->findOneBy(['token' => $token]);

        if ($tokenEntity === null) {
            throw new TokenNotFoundException();
        }

        $user = $tokenEntity->getOwner();
        $user->removeToken($tokenEntity);
        $user->setRegistrationComplete(true);

        $this->userRepository->save($user, true);
    }

    public function startPasswordReset(StartPasswordReset $startPasswordReset): void
    {
        $user = $this->userRepository->findOneBy(['email' => $startPasswordReset->getEmail()]);

        if ($user === null) {
            return;
        }

        $this->startPasswortResetForUser($user);
    }

    public function finishPasswordReset(FinishPasswordReset $dto): void
    {
        $tokenEntity = $this->tokenRepository->findOneBy(['token' => $dto->getToken()]);

        if ($tokenEntity === null) {
            throw new TokenNotFoundException();
        }

        $user = $tokenEntity->getOwner();
        $user->removeToken($tokenEntity);
        $user->setRegistrationComplete(true);

        $this->changePassword($user, $dto->getPassword());
        $this->userRepository->save($user, true);
    }

    public function startPasswortResetForUser(User $user): void
    {
        $token = $user->createToken();
        $this->userRepository->save($user, true);

        $this->mailerService->sendPasswordResetMail($user, $token);
    }

    public function changePassword(User $user, string $password)
    {
        if ($this->pwnedService->isPwned($password)) {
            throw new PasswordIsPwnedException();
        }

        $user->setPasswordHash($this->passwordHasher->hashPassword($user, $password));
    }
}
<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class WebfingerController extends AbstractController
{

    private UserRepository $userRepository;
    private RouterInterface $router;

    public function __construct(UserRepository $userRepository, RouterInterface $router)
    {
        $this->userRepository = $userRepository;
        $this->router = $router;
    }

    #[Route('/.well-known/webfinger', name: 'webfinger', methods: ['GET'])]
    public function webfingerLookup(Request $request): Response
    {
        $resource = $request->query->get('resource');

        if ($resource === null) {
            throw new BadRequestException('resource query param missing');
        }

        if (!\str_starts_with($resource, 'acct:')) {
            throw new NotFoundHttpException();

        }

        [$username, $domain] = explode('@', \substr($resource, 5), 2);

        if ($domain !== $request->getHttpHost()) {
            throw new NotFoundHttpException();
        }

        $user = $this->userRepository->findOneByUsername($username);

        if ($user === null) {
            throw new NotFoundHttpException();
        }

        $profileUrl = $this->router->generate('user_profile', ['username' => $user->getUsername()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse([
            'subject' => \sprintf('acct:%s@%s', $user->getUsername(), $request->getHttpHost()),
            'aliases' => [],
            'links' => [
                [
                    'rel' => 'self',
                    'type' => 'application/activity+json',
                    'href' => $profileUrl,
                ],
                [
                    'rel' => 'http://webfinger.net/rel/profile-page',
                    'type' => 'text/html',
                    'href' => $profileUrl,
                ]
            ]
        ]);

    }
}
<?php

namespace App\Repository;

use App\Entity\ExternalActor;
use App\Entity\ExternalFollowing;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ExternalFollowing>
 *
 * @method ExternalFollowing|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExternalFollowing|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExternalFollowing[]    findAll()
 * @method ExternalFollowing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExternalFollowingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExternalFollowing::class);
    }

    public function save(ExternalFollowing $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ExternalFollowing $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ExternalFollowing[] Returns an array of ExternalFollowing objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ExternalFollowing
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function isFollowing(User $follower, ExternalActor $followee)
    {
        $localFollow = $this->createQueryBuilder('ef')
            ->andWhere('ef.localFollower = :follower')
            ->setParameter('follower', $follower)
            ->andWhere('ef.externalActor = :followee')
            ->setParameter('followee', $followee)
            ->getQuery()
            ->getOneOrNullResult();
        return $localFollow !== null;
    }
}

<?php

namespace App\Repository;

use App\Entity\ExternalActor;
use App\Entity\ExternalFollower;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ExternalFollower>
 *
 * @method ExternalFollower|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExternalFollower|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExternalFollower[]    findAll()
 * @method ExternalFollower[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExternalFollowerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExternalFollower::class);
    }

    public function add(ExternalActor $externalActor, User $localFollowee, bool $flush = false): ExternalFollower
    {
        $entity = (new ExternalFollower())
            ->setExternalActor($externalActor)
            ->setLocalFollowee($localFollowee);

        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $entity;
    }

    public function remove(ExternalActor $externalActor, User $localFollowee): void
    {
        $this->_em->createQueryBuilder()
            ->delete('App:ExternalFollower', 'ef')
            ->andWhere('ef.externalActor = :follower')
            ->setParameter('follower', $externalActor)
            ->andWhere('ef.localFollowee = :followee')
            ->setParameter('followee', $localFollowee)
            ->getQuery()
            ->execute();
    }

//    /**
//     * @return ExternalFollower[] Returns an array of ExternalFollower objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ExternalFollower
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
